# Maintainer: Pierre Schmitz <pierre@archlinux.de>

pkgname=gd
pkgver=2.2.5
pkgrel=1.1
pkgdesc="Library for the dynamic creation of images by programmers"
arch=('x86_64')
url="https://libgd.github.io/"
license=('custom')
depends=('fontconfig' 'libxpm' 'libwebp')
optdepends=('perl: bdftogd script')
checkdepends=('ttf-liberation')
source=("https://github.com/libgd/libgd/releases/download/gd-${pkgver}/libgd-${pkgver}.tar.xz"
		'https://raw.githubusercontent.com/alpinelinux/aports/master/main/gd/CVE-2018-1000222.patch'
		'https://raw.githubusercontent.com/alpinelinux/aports/master/main/gd/CVE-2018-5711.patch'
		'https://raw.githubusercontent.com/alpinelinux/aports/master/main/gd/CVE-2019-6977.patch'
		'https://raw.githubusercontent.com/alpinelinux/aports/master/main/gd/CVE-2019-6978.patch'
)

md5sums=('8d8d6a6189513ecee6e893b1fb109bf8'
         'aea6db2d724c79b278f3f3e892a49868'
         '78b63c9f8b18c8223d9b7c27542d4617'
         '71deb2723ebdf83dc548572caf01bdb8'
         '24dd924e20f8016ae53fec65f7d66b3e')

prepare() {
  cd libgd-${pkgver}
  
  patch -Np1 -i ../CVE-2018-5711.patch
  patch -Np1 -i ../CVE-2018-1000222.patch
  patch -Np1 -i ../CVE-2019-6977.patch
  patch -Np1 -i ../CVE-2019-6978.patch
}

build() {
  cd libgd-${pkgver}
  ./configure \
    --prefix=/usr \
    --disable-rpath
  make
}

check() {
  cd libgd-${pkgver}
  # see https://github.com/libgd/libgd/issues/302
  [[ ${CARCH} == 'i686' ]] || FREETYPE_PROPERTIES='truetype:interpreter-version=35' make check
}

package() {
  cd libgd-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -D -m644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}
